var classhb_1_1network_1_1_hb_general_config =
[
    [ "HbGeneralConfig", "classhb_1_1network_1_1_hb_general_config.html#ab0657729c643feb1885d2a712a55685b", null ],
    [ "HbGeneralConfig", "classhb_1_1network_1_1_hb_general_config.html#ae91e5e77278a655c35d144ebc6ced7d1", null ],
    [ "~HbGeneralConfig", "classhb_1_1network_1_1_hb_general_config.html#a8024a323d6e8dd4088e5fb1163985d4f", null ],
    [ "appName", "classhb_1_1network_1_1_hb_general_config.html#ab06b9247ae869408bd04ceaa1a40c0e4", null ],
    [ "isValid", "classhb_1_1network_1_1_hb_general_config.html#a3615dce260c7b3caae25d0fb6c9a7805", null ],
    [ "operator=", "classhb_1_1network_1_1_hb_general_config.html#a44901f21bf09a550d772ccb80cd75e18", null ],
    [ "protocolVersion", "classhb_1_1network_1_1_hb_general_config.html#a07e6b58ec61e9dcd47a01623daed8e6d", null ],
    [ "setAppName", "classhb_1_1network_1_1_hb_general_config.html#a02550ab1175b608e61b6e7725b8092e4", null ],
    [ "setProtocolVersion", "classhb_1_1network_1_1_hb_general_config.html#a53ac72c8f614fb3fecf2d928eeb3ba65", null ]
];
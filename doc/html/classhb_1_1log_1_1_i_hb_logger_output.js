var classhb_1_1log_1_1_i_hb_logger_output =
[
    [ "OutputType", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325daf", [
      [ "OUTPUT_CONSOLE", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafa723d3911882fa33af8c08c7b32de3d99", null ],
      [ "OUTPUT_GUI", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafaba9cfb4978266688a64e6327d4a8440e", null ],
      [ "OUTPUT_FILE", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafac12f9040e13fc67a54a0a663f7c50608", null ],
      [ "OUTPUT_LOCAL_SOCKET", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafaed14a3dba66548c0717ec0f02a7e518d", null ],
      [ "OUTPUT_TCP_SOCKET", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafa4dbc4bf2cbc9ab72f28f6ea5b1611732", null ],
      [ "OUTPUT_UDP_SOCKET", "classhb_1_1log_1_1_i_hb_logger_output.html#ad5c3ae3f20035976d0fef28d67325dafa69a22ebc1c054d3109144318ad05ab5e", null ]
    ] ],
    [ "~IHbLoggerOutput", "classhb_1_1log_1_1_i_hb_logger_output.html#a3a2ebeb63a40c1f04403121a25eb1755", null ],
    [ "level", "classhb_1_1log_1_1_i_hb_logger_output.html#aebd939e9ae4d65760ec7b62288d6222c", null ],
    [ "setLevel", "classhb_1_1log_1_1_i_hb_logger_output.html#accf59b0cc55fefdbf1b39771cab34faa", null ],
    [ "type", "classhb_1_1log_1_1_i_hb_logger_output.html#a97aba60eaed254a9bb3742f235cbaaf3", null ]
];
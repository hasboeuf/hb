var classhb_1_1log_1_1_i_hb_logger_input =
[
    [ "InputType", "classhb_1_1log_1_1_i_hb_logger_input.html#afe162b2255d0a69163ac209578828550", [
      [ "INPUT_LOCAL_SOCKET", "classhb_1_1log_1_1_i_hb_logger_input.html#afe162b2255d0a69163ac209578828550a26e69c210532513707e139280c04f082", null ],
      [ "INPUT_TCP_SOCKET", "classhb_1_1log_1_1_i_hb_logger_input.html#afe162b2255d0a69163ac209578828550ab6ad61417bb8e1b00189285cb153eeb4", null ],
      [ "INPUT_UDP_SOCKET", "classhb_1_1log_1_1_i_hb_logger_input.html#afe162b2255d0a69163ac209578828550a1de5a7a7324aafee158be9cb6c695666", null ]
    ] ],
    [ "~IHbLoggerInput", "classhb_1_1log_1_1_i_hb_logger_input.html#a14a13e59afa3ebca828f445b0f1e4af8", null ],
    [ "type", "classhb_1_1log_1_1_i_hb_logger_input.html#acfead28a24d170ed89017df43ff48373", null ]
];
var classhb_1_1plugin_1_1_i_hb_plugin =
[
    [ "PluginInitState", "classhb_1_1plugin_1_1_i_hb_plugin.html#a5644ea196f26216a89c11892d33b242e", [
      [ "INIT_SUCCESS", "classhb_1_1plugin_1_1_i_hb_plugin.html#a5644ea196f26216a89c11892d33b242ea4b8f7c1b860b75bc4195e921c2a1008b", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1plugin_1_1_i_hb_plugin.html#a5644ea196f26216a89c11892d33b242ea102605b59257c14297e333e6a51256fe", null ],
      [ "INIT_FAIL", "classhb_1_1plugin_1_1_i_hb_plugin.html#a5644ea196f26216a89c11892d33b242eab648ce158db5eeee39f80b361204fe93", null ]
    ] ],
    [ "IHbPlugin", "classhb_1_1plugin_1_1_i_hb_plugin.html#a33828262c3f1c7208a7f42848fdcb4d2", null ],
    [ "~IHbPlugin", "classhb_1_1plugin_1_1_i_hb_plugin.html#a043fb45471c84bc23e3392d0fe3a2eef", null ],
    [ "init", "classhb_1_1plugin_1_1_i_hb_plugin.html#a52771292824cd1a1c97fef2d991ce1fc", null ],
    [ "unload", "classhb_1_1plugin_1_1_i_hb_plugin.html#a61afe0bf9a70bc304d220c8134482056", null ]
];
var classhb_1_1log_1_1_hb_logger_outputs =
[
    [ "State", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831f", [
      [ "INOUT_ADD_SUCCESS", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fab4942dc13d532faa2799a0bf9cdeacb2", null ],
      [ "INOUT_WRONG_PARAMETERS", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fa845736acb8dc0c2846f96496878e0c62", null ],
      [ "INOUT_ALREADY_EXISTS", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fa6dc53382c4c5b98df4890fc3047da163", null ],
      [ "INOUT_CONSOLE_ALREADY_EXISTS", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fa14eeafe49158db956906489865e8c1f4", null ],
      [ "INOUT_DEL_SUCCESS", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fa96b0d75aea7ce91acb648632dad520bb", null ],
      [ "INOUT_DEL_FAIL", "classhb_1_1log_1_1_hb_logger_outputs.html#a3547a18cc59151406dcfe620e00c831fa21e4443fde4d9d6df21859c7ba88c8f8", null ]
    ] ],
    [ "addConsoleOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#aab27f6f294a3c700c715b0249bd9343d", null ],
    [ "addFileOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#adbb4edd5c1cf6e44ff9c5e5cb066720d", null ],
    [ "addGuiOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#a9546b53351f15ac226e09faf77e36efb", null ],
    [ "addLocalSocketOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#ac7a20bfef9a3af5b50d221bdd9555e03", null ],
    [ "addTcpSocketOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#a43431e0de18187eeb3f53f5a7b3efaca", null ],
    [ "addUdpSocketOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#afa5a322a208658d2926f275e41290f38", null ],
    [ "output", "classhb_1_1log_1_1_hb_logger_outputs.html#adf11783920790028d7e2420d2a017f0b", null ],
    [ "removeOutput", "classhb_1_1log_1_1_hb_logger_outputs.html#a734216301c5199f04d4d2df5fd16d139", null ]
];
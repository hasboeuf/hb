var classhb_1_1plugin_1_1_hb_plugin_interface =
[
    [ "PluginInitState", "classhb_1_1plugin_1_1_hb_plugin_interface.html#ab275a7e2c401106de7f83176fc79dda0", [
      [ "INIT_SUCCESS", "classhb_1_1plugin_1_1_hb_plugin_interface.html#ab275a7e2c401106de7f83176fc79dda0a423fab6b287e2d50ccd8606859395911", null ],
      [ "INIT_SUCCESS_PARTLY", "classhb_1_1plugin_1_1_hb_plugin_interface.html#ab275a7e2c401106de7f83176fc79dda0a9c8e101f9b33a7f74dd184b5dd68b5bb", null ],
      [ "INIT_FAIL", "classhb_1_1plugin_1_1_hb_plugin_interface.html#ab275a7e2c401106de7f83176fc79dda0a5d0fda0ca05f69f7dd550d13e6c42291", null ]
    ] ],
    [ "~HbPluginInterface", "classhb_1_1plugin_1_1_hb_plugin_interface.html#ab7a5ce4797dfc567377b5c70e7e3b760", null ],
    [ "init", "classhb_1_1plugin_1_1_hb_plugin_interface.html#af966251d500f045225ebbbaae81f1706", null ],
    [ "unload", "classhb_1_1plugin_1_1_hb_plugin_interface.html#af09b5f8dc72865f4ded23a1f1b492ebe", null ]
];
var classhb_1_1pluginexample_1_1_app_abstract_plugin =
[
    [ "PluginInitState", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242e", [
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ],
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ]
    ] ],
    [ "PluginInitState", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242e", [
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ],
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ]
    ] ],
    [ "AppAbstractPlugin", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a3f35085c403ef9b609ed760b39dd2bc2", null ],
    [ "~AppAbstractPlugin", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#ad82b5c20dbc4882be302cd2a1bf7db8b", null ],
    [ "AppAbstractPlugin", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a7a1ab005103eccbba1c7bab37cb2c80f", null ],
    [ "~AppAbstractPlugin", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a3e20a60911a6b1003770f829b8e8b002", null ],
    [ "init", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a120645915a617109ad5e5ad25ced341c", null ],
    [ "init", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a3ad1b368d79a59b62e92a433302de228", null ],
    [ "unload", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a653b2ec9e6da1e20f706aa064d1982fe", null ],
    [ "unload", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a653b2ec9e6da1e20f706aa064d1982fe", null ],
    [ "mpPlatformService", "classhb_1_1pluginexample_1_1_app_abstract_plugin.html#a0da1e45476683268f29f30980b6eef2b", null ]
];
var classhb_1_1logviewer_1_1_log_viewer_config =
[
    [ "LogViewerConfig", "classhb_1_1logviewer_1_1_log_viewer_config.html#add8f3cd3e01c530997a3c24df1145b70", null ],
    [ "LogViewerConfig", "classhb_1_1logviewer_1_1_log_viewer_config.html#ac8adc44780b6d06df03e16448d8ac4a9", null ],
    [ "~LogViewerConfig", "classhb_1_1logviewer_1_1_log_viewer_config.html#ac04551c6d840a1efe94903e6e2a9d97d", null ],
    [ "addEditor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a4f90707f60b415ed38962e9f408b9f1b", null ],
    [ "addProjectFolder", "classhb_1_1logviewer_1_1_log_viewer_config.html#a1fdbad1955a60d63255dbae3418cdbda", null ],
    [ "backgroundColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a3ee0ca17561155f5ca619428e3e6fe51", null ],
    [ "backgroundColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a6d471d879bbcd42fba8cfbb221cbee52", null ],
    [ "buildConfigFromDom", "classhb_1_1logviewer_1_1_log_viewer_config.html#a835df8cfc9bc7a69034c2d86e6c99d9b", null ],
    [ "buildDomFromConfig", "classhb_1_1logviewer_1_1_log_viewer_config.html#a06cfe1766959c5154b91aedc497f5246", null ],
    [ "colorByIdLevel", "classhb_1_1logviewer_1_1_log_viewer_config.html#a99ddaa07b521f27ed67d357aa5dcceff", null ],
    [ "colorByIdLevel", "classhb_1_1logviewer_1_1_log_viewer_config.html#abdda764828bc97731f96763cc2f7eb80", null ],
    [ "defaultEditor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a420ce658b7545ce082d369be1f28def8", null ],
    [ "editorCommand", "classhb_1_1logviewer_1_1_log_viewer_config.html#a0b4e050f008dca768fdb3b1eaf119fe9", null ],
    [ "editors", "classhb_1_1logviewer_1_1_log_viewer_config.html#a3fe09cb02c814ebd5b6bd9ecec66b868", null ],
    [ "font", "classhb_1_1logviewer_1_1_log_viewer_config.html#a301d430f632c85ae7375cfb21ed8229d", null ],
    [ "font", "classhb_1_1logviewer_1_1_log_viewer_config.html#adeffb3705add10e7a0cfa6522af3f1b1", null ],
    [ "isValid", "classhb_1_1logviewer_1_1_log_viewer_config.html#a6054eb1b04da5b356625a1bf9fb4bff9", null ],
    [ "isValid", "classhb_1_1logviewer_1_1_log_viewer_config.html#a5bd2dab20f45d049d963409a058863b0", null ],
    [ "levelColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a936bf0a22e3ebc26f63184fb813738f1", null ],
    [ "levelColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#ac508f3a8bea88749e218642e404960f0", null ],
    [ "loadSettings", "classhb_1_1logviewer_1_1_log_viewer_config.html#af9c57a117fda7c98c101a4c78f1e9e69", null ],
    [ "maxBuffer", "classhb_1_1logviewer_1_1_log_viewer_config.html#ac0b1bd7c15a1893893dc37c91fc40a51", null ],
    [ "maxBuffer", "classhb_1_1logviewer_1_1_log_viewer_config.html#aa8df2538a7a2d175eb9a7935817be34e", null ],
    [ "operator=", "classhb_1_1logviewer_1_1_log_viewer_config.html#a6ca7d51c179ca73a761c95214bc85739", null ],
    [ "projectFolders", "classhb_1_1logviewer_1_1_log_viewer_config.html#aa2cceb834d4c8d24f98f3333afd3963d", null ],
    [ "resetEditors", "classhb_1_1logviewer_1_1_log_viewer_config.html#a8928f0104cea72822242853b571f8f11", null ],
    [ "resetProjectFolders", "classhb_1_1logviewer_1_1_log_viewer_config.html#a9a3114d00b57045258400d51650b6341", null ],
    [ "saveSettings", "classhb_1_1logviewer_1_1_log_viewer_config.html#ade0a2cf47ea2591c724a3d76d49d4d84", null ],
    [ "setBackgroundColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#aff06e66bfa924229c8a394836c0deae9", null ],
    [ "setBackgroundColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#af19b752b4c2cb268bbdbae5bb8f25ead", null ],
    [ "setColorById", "classhb_1_1logviewer_1_1_log_viewer_config.html#a2fed248d570cef4d6de2d2b953bbcda7", null ],
    [ "setColorById", "classhb_1_1logviewer_1_1_log_viewer_config.html#a3746ba9aab1afa28d076918eb8a4d4d1", null ],
    [ "setDefaultEditor", "classhb_1_1logviewer_1_1_log_viewer_config.html#a7fe907aafdc1f9e099c5badcefff9065", null ],
    [ "setFont", "classhb_1_1logviewer_1_1_log_viewer_config.html#a6f118c627f74367571a265dcd74b3a5a", null ],
    [ "setFont", "classhb_1_1logviewer_1_1_log_viewer_config.html#a98bca62c231f148f020d71972ffefc27", null ],
    [ "setMaxBuffer", "classhb_1_1logviewer_1_1_log_viewer_config.html#a6b0e7ae40fa23549eb664ec271195f29", null ],
    [ "setMaxBuffer", "classhb_1_1logviewer_1_1_log_viewer_config.html#a768478fdee1917cdcf2cbac2fed24495", null ],
    [ "mBackgroundColor", "classhb_1_1logviewer_1_1_log_viewer_config.html#ae8bf5fe26465a6ce9633f8ce30dbdf16", null ],
    [ "mFont", "classhb_1_1logviewer_1_1_log_viewer_config.html#ad09732f64f49ae3651095a402bbc2795", null ],
    [ "mLevelColors", "classhb_1_1logviewer_1_1_log_viewer_config.html#a459e5106e41e8d6f008e9017f691ddf6", null ],
    [ "mMaxBuffer", "classhb_1_1logviewer_1_1_log_viewer_config.html#a69f22e8b88e50708617ff6806d5607b2", null ]
];
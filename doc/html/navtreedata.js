var NAVTREE =
[
  [ "hb", "index.html", [
    [ "Todo List", "todo.html", null ],
    [ "Bug List", "bug.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_hb_abstract_client_8h.html",
"_hb_o2_config_8h.html#a835cf96474bab3ca7a981bbf849e64dd",
"classhb_1_1link_1_1_hb_o2_client_config.html#a46c0953b04ecd56e1b745317156e998b",
"classhb_1_1log_1_1_hb_log_file_output.html#ad5c3ae3f20035976d0fef28d67325dafaba9cfb4978266688a64e6327d4a8440e",
"classhb_1_1log_1_1_hb_logger_pool.html#a2ddc46256bccfdc2886c75cf090f87de",
"classhb_1_1network_1_1_hb_auth_status_contract.html#ab78f15c2d4e9c94e4fe85d000f48b857",
"classhb_1_1network_1_1_hb_general_client_config.html#a1eed098548fa8b4e8ff9ad50e409a560",
"classhb_1_1network_1_1_hb_network_user_info.html#a0b1dd6b2f0ab4526e85dcb137625426e",
"classhb_1_1network_1_1_hb_server_peopled_channel.html#a590ec44296d091668c8348adeac37d99",
"classhb_1_1network_1_1_hb_tcp_server_config.html#ab0f3f535305a480727ce8e95b9809e83",
"classhb_1_1tools_1_1_hb_timeout_network_reply.html#a9d19f60e8a051b586f6bd9ec029e5104"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
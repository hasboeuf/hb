var classhb_1_1network_1_1_hb_network_protocol =
[
    [ "AuthStatus", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15f", [
      [ "AUTH_INTERNAL_ERROR", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fac46ee31bd9d490607633db1478b0729e", null ],
      [ "AUTH_BAD", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fa40546eea05563dc0baa4fc9815935104", null ],
      [ "AUTH_BAD_ID", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15faf466a5a25f5c2276c7fce40ac5309020", null ],
      [ "AUTH_BAD_PASSWORD", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fa1afc0b78af8452a73cc4ec042a587ef0", null ],
      [ "AUTH_OK", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fa381ff102363fb359f84aa19af20e801e", null ],
      [ "AUTH_OK_ADMIN", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fa24516d972a16623e62e40e9a519cc5d6", null ],
      [ "AUTH_FB_KO", "classhb_1_1network_1_1_hb_network_protocol.html#af3aaf803040acf968fde338eeee6d15fa6b4af36afc1faacde748cd023993b545", null ]
    ] ],
    [ "ClientStatus", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942", [
      [ "CLIENT_DISCONNECTED", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942a0259cdb4d9ed2d4798e55a7aceec79eb", null ],
      [ "CLIENT_CONNECTING", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942a4b68974116a11c6faa2d4b06bc12e7cd", null ],
      [ "CLIENT_CONNECTED", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942a20f5ec2cc0f690412f3780164aadad16", null ],
      [ "CLIENT_AUTHENTICATING", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942adf1430820506747f5af73b6d9a700c1e", null ],
      [ "CLIENT_AUTHENTICATED", "classhb_1_1network_1_1_hb_network_protocol.html#a8d5fc97321d5906447f471a562384942a5df7535e7fe9423fa21519ea354f85c7", null ]
    ] ],
    [ "Code", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922", [
      [ "CODE_UNDEFINED", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922afcd55f5a347532f9b57d458a84ed3800", null ],
      [ "CODE_CLT_AUTH_REQUEST", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922a8e1aa2f3a0468b18afbde5fe0211deb7", null ],
      [ "CODE_SRV_AUTH_STATUS", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922a03c4d8a0c25c57ae1e9b7d246afac4bc", null ],
      [ "CODE_CLT_PRESENCE", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922a5c6360653459d467c5534ed385c0b76c", null ],
      [ "CODE_SRV_PRESENCE_STATUS", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922ad8be4bc2ae8039256d00c46a6ed1491f", null ],
      [ "CODE_SRV_KICK", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922a39a9b538fb78e9a3de53e4e646359542", null ],
      [ "CODE_USER", "classhb_1_1network_1_1_hb_network_protocol.html#a1b3be0d1f3ec5e75cf8973691b838922ae37a07c8a93f84424da213b9f898f161", null ]
    ] ],
    [ "KickCode", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9", [
      [ "KICK_UNDEFINED", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9adf162cff22fc9e471deb9e2baa2b9241", null ],
      [ "KICK_INTERNAL_ERROR", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9a2c3bfe9231eeac0be926f14b93c95cae", null ],
      [ "KICK_PROTOCOL_DIFFERENT", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9a5c115a06cd08853687a910b750aaa5a9", null ],
      [ "KICK_CONTRACT_INVALID", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9ac5e5e80de04823cb400c1b0c38b42cb5", null ],
      [ "KICK_AUTH_LIMIT", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9a65f9e7c8e29257c66c823d39e1ee4344", null ],
      [ "KICK_AUTH_TIMEOUT", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9a86cc740a145dd2a07f5f4a57145c8661", null ],
      [ "KICK_PRESENCE_TIMEOUT", "classhb_1_1network_1_1_hb_network_protocol.html#a745a3d903e204817e731ad5ad530e4d9af91a6dd64190b2961e72d657a65bd864", null ]
    ] ],
    [ "NetworkType", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092c", [
      [ "NETWORK_UNDEFINED", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092ca87fd35fe456e4e56e758fa29c55fa9ef", null ],
      [ "NETWORK_TCP", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092caec04f350a1c295c699dfa99b0f65cef6", null ],
      [ "NETWORK_WEB", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092ca3294b0de7eb10b3c81929fda151f995e", null ],
      [ "NETWORK_SSL", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092ca9cba52a1a914c1a4e033e634c5e71412", null ],
      [ "NETWORK_UDP", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092ca6bf235aa5d934954197f98525ec1fd48", null ],
      [ "NETWORK_LOCAL", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092ca7eb45039051b94ee817bfc18ef9680fe", null ],
      [ "NETWORK_BLUETOOTH", "classhb_1_1network_1_1_hb_network_protocol.html#a72ec6f56bd5ad79c72855b970986092cacee891eaa7b8c2e529b5836e563093d8", null ]
    ] ],
    [ "RoutingScheme", "classhb_1_1network_1_1_hb_network_protocol.html#ad60e579461557933a5382efbff80f785", [
      [ "ROUTING_UNDEFINED", "classhb_1_1network_1_1_hb_network_protocol.html#ad60e579461557933a5382efbff80f785a7c81ee934cd3c178e61be6ff8dbf0889", null ],
      [ "ROUTING_UNICAST", "classhb_1_1network_1_1_hb_network_protocol.html#ad60e579461557933a5382efbff80f785a59005cd74f8c1094ad8325da80de8bcf", null ],
      [ "ROUTING_MULTICAST", "classhb_1_1network_1_1_hb_network_protocol.html#ad60e579461557933a5382efbff80f785a377505f3a163274d17644d9109c1a6ba", null ],
      [ "ROUTING_BROADCAST", "classhb_1_1network_1_1_hb_network_protocol.html#ad60e579461557933a5382efbff80f785ad0669ab49e4dd425f05236e481c94384", null ]
    ] ],
    [ "ServerStatus", "classhb_1_1network_1_1_hb_network_protocol.html#a7e15fba78403aa61fd44d4c062e0928e", [
      [ "SERVER_DISCONNECTED", "classhb_1_1network_1_1_hb_network_protocol.html#a7e15fba78403aa61fd44d4c062e0928ead946f209f08f81808e9f6d876f3cb454", null ],
      [ "SERVER_LISTENING", "classhb_1_1network_1_1_hb_network_protocol.html#a7e15fba78403aa61fd44d4c062e0928eac1b8c1dcae65525caee1ef7cc1be6602", null ]
    ] ],
    [ "Service", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1", [
      [ "SERVICE_UNDEFINED", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1a09903052771b0774fe03d89e685acb70", null ],
      [ "SERVICE_UPDATE", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1a18b9e6cbc054158d6acc4f72740cf5eb", null ],
      [ "SERVICE_KICK", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1a1ccffd31c58b128a58983e35c4a2e8f1", null ],
      [ "SERVICE_AUTH", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1a7027bbf8ad2ec528ede37fca7a8f7988", null ],
      [ "SERVICE_PRESENCE", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1af6a8fd0d8ed1b598fa7973c8e02cc358", null ],
      [ "SERVICE_CHANNEL", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1ab787c298525f3b8549550f16b5686599", null ],
      [ "SERVICE_USER", "classhb_1_1network_1_1_hb_network_protocol.html#a2170741d9ac332810900a85db91456f1a944cae503bbdf827f99864e5b99e18ee", null ]
    ] ],
    [ "UserStatus", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404db", [
      [ "USER_DISCONNECTED", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404dba7a216520bd049aed6186558d63eba9bd", null ],
      [ "USER_CONNECTING", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404dba8b17c5c2ddfe9e702a8ca1bfe6e6e0e8", null ],
      [ "USER_CONNECTED", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404dba821613d43874ed49f1d155d792ed864d", null ],
      [ "USER_AUTHENTICATING", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404dbad8333ef15047bdc4f1c06166c4164b81", null ],
      [ "USER_AUTHENTICATED", "classhb_1_1network_1_1_hb_network_protocol.html#a07c443f167c33107ea51ec95e32404dba3a8f9cccb91289e66bf20c6c636643e0", null ]
    ] ]
];
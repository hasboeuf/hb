var classhb_1_1pluginexample_1_1_plugin2 =
[
    [ "PluginInitState", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242e", [
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ],
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ]
    ] ],
    [ "PluginInitState", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242e", [
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ],
      [ "INIT_SUCCESS", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242eabb338a332320a11b6b92bde7117bd4e2", null ],
      [ "INIT_SUCCESS_PARTIALLY", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ea2604540a6fbd499ae15324371705aa30", null ],
      [ "INIT_FAIL", "classhb_1_1pluginexample_1_1_plugin2.html#a5644ea196f26216a89c11892d33b242ead8fc3bdf30e2f598c84468e85e34000b", null ]
    ] ],
    [ "Plugin2", "classhb_1_1pluginexample_1_1_plugin2.html#a36a35decfde5cf79406fac08ffaa112a", null ],
    [ "~Plugin2", "classhb_1_1pluginexample_1_1_plugin2.html#a5ca5b0b17187817a4b621b258893d76e", null ],
    [ "Plugin2", "classhb_1_1pluginexample_1_1_plugin2.html#ab1078b7c8aff8e61ff536d67400fc5ab", null ],
    [ "~Plugin2", "classhb_1_1pluginexample_1_1_plugin2.html#ad58a7c9495f10a064ab218b1a38a9b62", null ],
    [ "doSomething", "classhb_1_1pluginexample_1_1_plugin2.html#ae96e5dea359ed94c9354285d5159aff7", null ],
    [ "doSomething", "classhb_1_1pluginexample_1_1_plugin2.html#a106d523795ca5552d39afaade4a50308", null ],
    [ "init", "classhb_1_1pluginexample_1_1_plugin2.html#ad17f3758862d78e6396d948c25dff227", null ],
    [ "init", "classhb_1_1pluginexample_1_1_plugin2.html#a537fd3ba5477bed28f0437a24ec8a250", null ],
    [ "onAction1Triggered", "classhb_1_1pluginexample_1_1_plugin2.html#afdc371f305b2a601b97854f3a414201e", null ],
    [ "onAction1Triggered", "classhb_1_1pluginexample_1_1_plugin2.html#a262a6cfbcb8ad98447309d47ea808ed9", null ],
    [ "unload", "classhb_1_1pluginexample_1_1_plugin2.html#a7a0ce737d13b8e7a78beb043af645985", null ],
    [ "unload", "classhb_1_1pluginexample_1_1_plugin2.html#ad297e171a551459bf237166eee6340ef", null ],
    [ "mpPlatformService", "classhb_1_1pluginexample_1_1_plugin2.html#a0da1e45476683268f29f30980b6eef2b", null ]
];
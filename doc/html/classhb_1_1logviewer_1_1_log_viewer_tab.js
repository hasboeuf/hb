var classhb_1_1logviewer_1_1_log_viewer_tab =
[
    [ "ColumnId", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316", [
      [ "COLUMN_LEVEL", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316a0288ffeb29174aa9d2d792e58bd1257d", null ],
      [ "COLUMN_TIME", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316a75739b5628b140cf8102c4323d66e0e6", null ],
      [ "COLUMN_OWNER", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316a971c4bbca7e5f64b8b6b48021df4b0b6", null ],
      [ "COLUMN_LINE", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316ac0815a9bc75a05307960c866c50b1d56", null ],
      [ "COLUMN_WHERE", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316aa54e7caf9c76c4802b32c6747641365a", null ],
      [ "COLUMN_TEXT", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316aa131c941604d77be0753f9f1f7f16c7f", null ],
      [ "COLUMN_FILE", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316ab7d858df13e8dfff206108c4d599f283", null ],
      [ "COLUMN_FUNC", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a69f5be7f7601b228adbaa98d74eea316a2bd899870fd912b1eb3beb55301eada6", null ]
    ] ],
    [ "LogViewerTab", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a97966bc0faee6dac43d6b33d62dd9112", null ],
    [ "LogViewerTab", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a5f823f8cfadde7ad549f583194bbb4bd", null ],
    [ "~LogViewerTab", "classhb_1_1logviewer_1_1_log_viewer_tab.html#ac4854776d29b32a4824940486661f15a", null ],
    [ "addEntry", "classhb_1_1logviewer_1_1_log_viewer_tab.html#acdc90488ba547f01b2617825ca389a94", null ],
    [ "id", "classhb_1_1logviewer_1_1_log_viewer_tab.html#acc1a0e36d83e2fded86f6b2dcb3ce62e", null ],
    [ "newTabRequest", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a58ed2bd6ad003e84816c4e54b2f2a4c6", null ],
    [ "onAdjustToContent", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a2f9d4d425c60b01a337b94f113c6d189", null ],
    [ "onClearClicked", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a188ed430df5cf444c2d41e85d8fbd671", null ],
    [ "onCopyCellContent", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a6490a9667c9f6db7d18aa5844a9eec32", null ],
    [ "onCustomContextMenuRequested", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a7abfc682683d911f6db0a7b32ec977e8", null ],
    [ "onFilterTextChanged", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a05d5cb8d5cb3bca58a930be1b0948761", null ],
    [ "onFreezeClicked", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a66611cb613c37fbc76ef1723c49bebdb", null ],
    [ "onLevelChanged", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a152b192f75a026654d7bcefb46db255b", null ],
    [ "onOpenNewTab", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a2ac69b4f32245b6fc2243328e02d80bb", null ],
    [ "onRowDoubleClicked", "classhb_1_1logviewer_1_1_log_viewer_tab.html#ab5a21541ff7ada3a05de489d8a5dcb1c", null ],
    [ "onSaveAsClicked", "classhb_1_1logviewer_1_1_log_viewer_tab.html#ad40c1fbc26b609f0ab5233ddeff1de73", null ],
    [ "updateView", "classhb_1_1logviewer_1_1_log_viewer_tab.html#aeff5941689466a11568141066d44c34c", null ],
    [ "_freezeAnimationValue", "classhb_1_1logviewer_1_1_log_viewer_tab.html#a6e2d62ec4b23f28b17cc43be87d4333b", null ]
];
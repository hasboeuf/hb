var classhb_1_1link_1_1_hb_facebook_object =
[
    [ "ObjectType", "classhb_1_1link_1_1_hb_facebook_object.html#aa07379a048b22c6770b14d95261f5b28", [
      [ "OBJECT_NONE", "classhb_1_1link_1_1_hb_facebook_object.html#aa07379a048b22c6770b14d95261f5b28af5e233178b3d686f6b8ad83cedb4fb21", null ],
      [ "OBJECT_USER", "classhb_1_1link_1_1_hb_facebook_object.html#aa07379a048b22c6770b14d95261f5b28a1a9320ef4c703f6a35594b6ea392ce24", null ]
    ] ],
    [ "HbFacebookObject", "classhb_1_1link_1_1_hb_facebook_object.html#a7f4d29e992db686dcc37f32d9592057f", null ],
    [ "~HbFacebookObject", "classhb_1_1link_1_1_hb_facebook_object.html#afe9312c6e2651d9275d749a75d417996", null ],
    [ "id", "classhb_1_1link_1_1_hb_facebook_object.html#a3414dc0f1fd2cd053cd08861d35c0b06", null ],
    [ "load", "classhb_1_1link_1_1_hb_facebook_object.html#a70b3d0efa63fc34976704d6d1aed4e27", null ],
    [ "setId", "classhb_1_1link_1_1_hb_facebook_object.html#a68abeec2afd01df7b55b4713a293f675", null ],
    [ "toString", "classhb_1_1link_1_1_hb_facebook_object.html#ac232052934b81b8163324ce340dc0ed6", null ],
    [ "type", "classhb_1_1link_1_1_hb_facebook_object.html#a862469e88c891551ae1fa9c5712481ea", null ],
    [ "mId", "classhb_1_1link_1_1_hb_facebook_object.html#a8d435e0d1d5c83e56ff86d49c2555035", null ],
    [ "mType", "classhb_1_1link_1_1_hb_facebook_object.html#a60dd6619c040c536c23f156ef2a49fc4", null ]
];
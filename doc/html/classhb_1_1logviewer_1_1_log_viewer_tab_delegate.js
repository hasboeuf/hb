var classhb_1_1logviewer_1_1_log_viewer_tab_delegate =
[
    [ "LogViewerTabDelegate", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#a37f16b13a4e73f7dbedefff15f3006ee", null ],
    [ "~LogViewerTabDelegate", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#a4448d00334c28bb2c4e8acd5ff171d18", null ],
    [ "createEditor", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#aa31ccee9c088bae727cbf6d0c24e3ff1", null ],
    [ "paint", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#ae60f8a6be542df5a81d53f5bfafed7d8", null ],
    [ "setEditorData", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#aedd9726440af878d7e604f9a23cf2e1e", null ],
    [ "setModelData", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#ab9a6ccbeee92ca4fcfa1fa7a09c20455", null ],
    [ "updateEditorGeometry", "classhb_1_1logviewer_1_1_log_viewer_tab_delegate.html#a075cfc1dd50db6a0c62b5fdbd74fb304", null ]
];
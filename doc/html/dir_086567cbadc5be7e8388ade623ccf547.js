var dir_086567cbadc5be7e8388ade623ccf547 =
[
    [ "auth", "dir_d3b4b088f19cff5d5a48ce578ae97999.html", "dir_d3b4b088f19cff5d5a48ce578ae97999" ],
    [ "general", "dir_de07dedd15f7950302ab7230c764cfe2.html", "dir_de07dedd15f7950302ab7230c764cfe2" ],
    [ "presence", "dir_9051f8b40fee0548c45ae88adc95a83a.html", "dir_9051f8b40fee0548c45ae88adc95a83a" ],
    [ "HbNetworkContract.h", "_hb_network_contract_8h.html", "_hb_network_contract_8h" ],
    [ "HbNetworkExchanges.h", "_hb_network_exchanges_8h.html", [
      [ "HbNetworkExchanges", "classhb_1_1network_1_1_hb_network_exchanges.html", "classhb_1_1network_1_1_hb_network_exchanges" ]
    ] ],
    [ "HbNetworkHeader.h", "_hb_network_header_8h.html", "_hb_network_header_8h" ],
    [ "HbNetworkProtocol.h", "_hb_network_protocol_8h.html", [
      [ "HbNetworkProtocol", "classhb_1_1network_1_1_hb_network_protocol.html", "classhb_1_1network_1_1_hb_network_protocol" ]
    ] ]
];
var classhb_1_1log_1_1_hb_logger_inputs =
[
    [ "State", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831f", [
      [ "INOUT_ADD_SUCCESS", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fab4942dc13d532faa2799a0bf9cdeacb2", null ],
      [ "INOUT_WRONG_PARAMETERS", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fa845736acb8dc0c2846f96496878e0c62", null ],
      [ "INOUT_ALREADY_EXISTS", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fa6dc53382c4c5b98df4890fc3047da163", null ],
      [ "INOUT_CONSOLE_ALREADY_EXISTS", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fa14eeafe49158db956906489865e8c1f4", null ],
      [ "INOUT_DEL_SUCCESS", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fa96b0d75aea7ce91acb648632dad520bb", null ],
      [ "INOUT_DEL_FAIL", "classhb_1_1log_1_1_hb_logger_inputs.html#a3547a18cc59151406dcfe620e00c831fa21e4443fde4d9d6df21859c7ba88c8f8", null ]
    ] ],
    [ "addLocalSocketInput", "classhb_1_1log_1_1_hb_logger_inputs.html#a85335dbbb0805514e7035ea11e2f4036", null ],
    [ "addTcpSocketInput", "classhb_1_1log_1_1_hb_logger_inputs.html#a36916e6c93e30a695e57153f36899d7e", null ],
    [ "addUdpSocketInput", "classhb_1_1log_1_1_hb_logger_inputs.html#a2b4c36c0f303abe782076fd56dfce9f6", null ],
    [ "input", "classhb_1_1log_1_1_hb_logger_inputs.html#abe9b559c01f0f6d551dfe6350f76b639", null ],
    [ "removeInput", "classhb_1_1log_1_1_hb_logger_inputs.html#a654b9bb34e1ff4c75051971659ea61d9", null ]
];
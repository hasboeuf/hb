var classhb_1_1plugin_1_1_hb_plugin_platform =
[
    [ "HbPluginPlatform", "classhb_1_1plugin_1_1_hb_plugin_platform.html#aae1738af739b202bd478373b7879e30d", null ],
    [ "~HbPluginPlatform", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a39ef1bd5675c10b5382942c7e411c5ab", null ],
    [ "isServiceRegistered", "classhb_1_1plugin_1_1_hb_plugin_platform.html#abce665969c5d91204331fcefa6e1aa4d", null ],
    [ "loadPlugins", "classhb_1_1plugin_1_1_hb_plugin_platform.html#aa1fa3dfc6dc9746bdf5ee2a06d1638c2", null ],
    [ "onLoadPluginRequest", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a84a966dee7650c76807c2188abbe797b", null ],
    [ "onPluginStateChanged", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a6609099ef15bc8449f4ea5b67708eae1", null ],
    [ "onUnloadPluginRequest", "classhb_1_1plugin_1_1_hb_plugin_platform.html#af997b9610ba98fb8a4abaae5195d7788", null ],
    [ "pluginInfoList", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a8d23b22b71c15ca87d9f0d40273a93bb", null ],
    [ "pluginStateChanged", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a9622c43b76f9d0360eb88b4abedb37b6", null ],
    [ "registerService", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a14cb142dc498103e04b68fb19d4db436", null ],
    [ "requestPlugin", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a302d1b0497d7189833b30360f0cff47e", null ],
    [ "requestService", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a6342850a8fca5d6b83da54bc716964d7", null ],
    [ "unloadPlugins", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a424caae7f35e420f4bb73e0fb4231be6", null ],
    [ "mPluginLoaded", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a06194d40b1bd65c7825bd73568df9043", null ],
    [ "mPluginManager", "classhb_1_1plugin_1_1_hb_plugin_platform.html#ae5254864caac5158519de1b14fa4839e", null ],
    [ "mServices", "classhb_1_1plugin_1_1_hb_plugin_platform.html#a8ea6d62310395c977d7d8a92cfd15080", null ]
];
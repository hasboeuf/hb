var classhb_1_1plugin_1_1_hb_plugin_list_widget =
[
    [ "ColumnId", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2", [
      [ "COLUMN_NAME", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2a8dfc929fe3e61acf185ff3e298a6a70e", null ],
      [ "COLUMN_LOAD", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2a8ae488c183564ee898b5adf4367f8c3d", null ],
      [ "COLUMN_VERSION", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2aa50fce73eeb8a5faed62c38fd7917868", null ],
      [ "COLUMN_AUTHOR", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2a0c6403e9425a1cbeaaac5fa807ab6706", null ],
      [ "COLUMN_REQUIRED", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2ab8dc34d6dbaab002109371651216e564", null ],
      [ "COLUMN_OPTIONAL", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ac8f1487fd20099ec69202b29167911a2ac18a073704cb271c0977ab72f045d59b", null ]
    ] ],
    [ "RoleId", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ab48e996cafd0101aece87c09800c9ced", [
      [ "ROLE_PLUGIN_NAME", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ab48e996cafd0101aece87c09800c9cedadc7a65dc98a6ef2e3cbb76f66e2d96c1", null ],
      [ "ROLE_PLUGIN_CHECKSTATE", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ab48e996cafd0101aece87c09800c9ceda0623395da6a0da20f924f4d442e86dec", null ]
    ] ],
    [ "HbPluginListWidget", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#aaa30954ca85e2533442e4e75edb4f84e", null ],
    [ "loadPluginRequest", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#a7783e7645c841e81d30a539d4828eee3", null ],
    [ "onPluginChecked", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#a3ef36c28cd193953a247f4769859c92d", null ],
    [ "onPluginStateChanged", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#ad77c7b2ba1abb75fb51c136133d003d9", null ],
    [ "unloadPluginRequest", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html#aeabbafc3f189414d38c4cbec3af24789", null ]
];
var class_ui_1_1_user_main_window =
[
    [ "retranslateUi", "class_ui_1_1_user_main_window.html#a6e58e4cc031dd35cc9d1471cc19b27e4", null ],
    [ "retranslateUi", "class_ui_1_1_user_main_window.html#a6e58e4cc031dd35cc9d1471cc19b27e4", null ],
    [ "retranslateUi", "class_ui_1_1_user_main_window.html#a6e58e4cc031dd35cc9d1471cc19b27e4", null ],
    [ "retranslateUi", "class_ui_1_1_user_main_window.html#a6e58e4cc031dd35cc9d1471cc19b27e4", null ],
    [ "retranslateUi", "class_ui_1_1_user_main_window.html#a6e58e4cc031dd35cc9d1471cc19b27e4", null ],
    [ "setupUi", "class_ui_1_1_user_main_window.html#afbd65c10354bc526231b64c24de489f8", null ],
    [ "setupUi", "class_ui_1_1_user_main_window.html#afbd65c10354bc526231b64c24de489f8", null ],
    [ "setupUi", "class_ui_1_1_user_main_window.html#afbd65c10354bc526231b64c24de489f8", null ],
    [ "setupUi", "class_ui_1_1_user_main_window.html#afbd65c10354bc526231b64c24de489f8", null ],
    [ "setupUi", "class_ui_1_1_user_main_window.html#afbd65c10354bc526231b64c24de489f8", null ],
    [ "centralwidget", "class_ui_1_1_user_main_window.html#a236c03e919e0b08a623ce3478627c25c", null ],
    [ "formLayout", "class_ui_1_1_user_main_window.html#a0fafdea03259aca72bc285bb26818567", null ],
    [ "horizontalLayout", "class_ui_1_1_user_main_window.html#a36e53da04caa70b02e87ab3dfa9de606", null ],
    [ "horizontalLayout_2", "class_ui_1_1_user_main_window.html#abc00725b525e135e1a454938554c80a7", null ],
    [ "horizontalLayout_3", "class_ui_1_1_user_main_window.html#afff10d3e605e3d630b4446a097b4f633", null ],
    [ "horizontalLayout_4", "class_ui_1_1_user_main_window.html#a42cef772bf692765106e315103d80121", null ],
    [ "horizontalLayout_5", "class_ui_1_1_user_main_window.html#ae09b7fbaaf7cc06cf7bd8077959def8f", null ],
    [ "horizontalLayout_6", "class_ui_1_1_user_main_window.html#a9e65946e20869aa3095e3697aa8134ea", null ],
    [ "horizontalSpacer", "class_ui_1_1_user_main_window.html#a57952d22fd5292d586e7e49af2e4cd90", null ],
    [ "horizontalSpacer_2", "class_ui_1_1_user_main_window.html#ac53c50df01de609a09d2276755f7d5d4", null ],
    [ "horizontalSpacer_3", "class_ui_1_1_user_main_window.html#afd8a986309880926bd5cd6806b9ff00b", null ],
    [ "label", "class_ui_1_1_user_main_window.html#a592c50742a178ac27acc21efb7b70746", null ],
    [ "label_2", "class_ui_1_1_user_main_window.html#a2fe57faf6930ecd5959da0a3ca66548d", null ],
    [ "label_3", "class_ui_1_1_user_main_window.html#a528bf2d727097b376e56ecc1de365edf", null ],
    [ "label_5", "class_ui_1_1_user_main_window.html#af416cca386ff5ae94fd5279a3fdc518a", null ],
    [ "menubar", "class_ui_1_1_user_main_window.html#aaeef547fb1d2a89b15395d8009983788", null ],
    [ "statusbar", "class_ui_1_1_user_main_window.html#abd14ca9ae16d5b0148aab01137e34541", null ],
    [ "tab", "class_ui_1_1_user_main_window.html#a426b638a25eace467ce55085482fbe99", null ],
    [ "tab_2", "class_ui_1_1_user_main_window.html#a8f5beee1ee22fef49cae5163be003f0c", null ],
    [ "tabWidget", "class_ui_1_1_user_main_window.html#ae90ab4d53422551da35ae3e54a093bc7", null ],
    [ "ui_qa_about", "class_ui_1_1_user_main_window.html#ab59dfd1adb8590bf1dd467bb4af6967c", null ],
    [ "ui_qa_exit", "class_ui_1_1_user_main_window.html#ad107170dfae73551bc980368093de8f5", null ],
    [ "ui_qa_help", "class_ui_1_1_user_main_window.html#adfd01bb2166480ff26d93b24b136f646", null ],
    [ "ui_qa_logs", "class_ui_1_1_user_main_window.html#a93b33ee28a3fbbf763fa646a3a3e346d", null ],
    [ "ui_qa_preferences", "class_ui_1_1_user_main_window.html#a482bd53def7d39f07a808bef6a0b5a81", null ],
    [ "ui_qle_text", "class_ui_1_1_user_main_window.html#ae49502bd7884ec510539d1fce6f7c45a", null ],
    [ "ui_qpb_compute", "class_ui_1_1_user_main_window.html#a17314e748de496fb79f9d8dc4840c2b2", null ],
    [ "ui_qpb_fb_authentication", "class_ui_1_1_user_main_window.html#a2d4d07e3f6353061f42ea3adaebfd2be", null ],
    [ "ui_qpb_fb_unauthentication", "class_ui_1_1_user_main_window.html#afabef4b424c50afb70296de22205c53b", null ],
    [ "ui_qpb_send", "class_ui_1_1_user_main_window.html#a8a8994159bff7cc0c4cbd475ee2db88e", null ],
    [ "ui_qpb_start", "class_ui_1_1_user_main_window.html#a183c861899ce39c4e4a6e5f7a570a9ba", null ],
    [ "ui_qpb_stop", "class_ui_1_1_user_main_window.html#a7883554bc2d3fb94d789aea604092d79", null ],
    [ "ui_qsb_a", "class_ui_1_1_user_main_window.html#a2af818e68086ae91db8ce960ec7e8328", null ],
    [ "ui_qsb_b", "class_ui_1_1_user_main_window.html#afa575fb6999ab899ef2a864c3fccb4a5", null ],
    [ "ui_qsb_sum", "class_ui_1_1_user_main_window.html#a3f12a1dffde6a33f9aa991f0bb904ca3", null ],
    [ "ui_qsw_stack", "class_ui_1_1_user_main_window.html#a609c8ba29e6276042e12e718169962cd", null ],
    [ "ui_qte_chat", "class_ui_1_1_user_main_window.html#a8282b8492db4762e8285a2fab8f985d2", null ],
    [ "ui_qte_console", "class_ui_1_1_user_main_window.html#abae1dbc922d2ebd6e2462c6ee6e22b8f", null ],
    [ "ui_qw_app", "class_ui_1_1_user_main_window.html#a8049bcd6abc66547b14ccb99d089148f", null ],
    [ "ui_qw_auth", "class_ui_1_1_user_main_window.html#a30fc90d2a3973e3949c92d814d449825", null ],
    [ "ui_qw_welcome", "class_ui_1_1_user_main_window.html#a6f5be3b71bd5bb218b1ae6f8f69e7557", null ],
    [ "verticalLayout", "class_ui_1_1_user_main_window.html#a2af2a39dde83f19d68186afaff69fd6b", null ],
    [ "verticalLayout_2", "class_ui_1_1_user_main_window.html#a5256b3f2f3dfe41f68028af12b6aecb9", null ],
    [ "verticalLayout_3", "class_ui_1_1_user_main_window.html#a81e01dac7f6a4001227ca735bea636f3", null ],
    [ "verticalLayout_4", "class_ui_1_1_user_main_window.html#a689b0060cf49da5ea82164e1a7823a1c", null ]
];
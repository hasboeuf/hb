var hierarchy =
[
    [ "hb::tools::HbApplicationHelper", "classhb_1_1tools_1_1_hb_application_helper.html", null ],
    [ "hb::network::HbClientAuthLoginObject", "classhb_1_1network_1_1_hb_client_auth_login_object.html", null ],
    [ "hb::tools::HbDictionaryHelper", "classhb_1_1tools_1_1_hb_dictionary_helper.html", null ],
    [ "hbprivate::HbEnumerator< Class >", "classhbprivate_1_1_hb_enumerator.html", null ],
    [ "hbprivate::HbEnumHelper< Class, Enum >", "classhbprivate_1_1_hb_enum_helper.html", null ],
    [ "hb::tools::HbErrorCode", "classhb_1_1tools_1_1_hb_error_code.html", null ],
    [ "hb::link::HbFacebookObject", "classhb_1_1link_1_1_hb_facebook_object.html", [
      [ "hb::link::HbFacebookUser", "classhb_1_1link_1_1_hb_facebook_user.html", null ]
    ] ],
    [ "hb::network::HbGeneralConfig", "classhb_1_1network_1_1_hb_general_config.html", [
      [ "hb::network::HbGeneralClientConfig", "classhb_1_1network_1_1_hb_general_client_config.html", null ],
      [ "hb::network::HbGeneralServerConfig", "classhb_1_1network_1_1_hb_general_server_config.html", null ]
    ] ],
    [ "HbLogConfigDialog", null, [
      [ "hb::log::HbLogConfigDialog", "classhb_1_1log_1_1_hb_log_config_dialog.html", null ]
    ] ],
    [ "hb::log::HbLogContext", "classhb_1_1log_1_1_hb_log_context.html", null ],
    [ "hb::log::HbLogEvent", "classhb_1_1log_1_1_hb_log_event.html", null ],
    [ "hb::log::HbLogger", "classhb_1_1log_1_1_hb_logger.html", [
      [ "hb::log::HbLogManager", "classhb_1_1log_1_1_hb_log_manager.html", null ]
    ] ],
    [ "hb::log::HbLogService", "classhb_1_1log_1_1_hb_log_service.html", null ],
    [ "HbLogWidget", null, [
      [ "hb::log::HbLogWidget", "classhb_1_1log_1_1_hb_log_widget.html", null ]
    ] ],
    [ "hb::network::HbNetworkConfig", "classhb_1_1network_1_1_hb_network_config.html", [
      [ "hb::network::HbClientConfig", "classhb_1_1network_1_1_hb_client_config.html", [
        [ "hb::network::HbTcpClientConfig", "classhb_1_1network_1_1_hb_tcp_client_config.html", null ]
      ] ],
      [ "hb::network::HbServerConfig", "classhb_1_1network_1_1_hb_server_config.html", [
        [ "hb::network::HbTcpServerConfig", "classhb_1_1network_1_1_hb_tcp_server_config.html", null ]
      ] ],
      [ "hb::network::HbTcpClientConfig", "classhb_1_1network_1_1_hb_tcp_client_config.html", null ],
      [ "hb::network::HbTcpConfig", "classhb_1_1network_1_1_hb_tcp_config.html", [
        [ "hb::network::HbTcpClientConfig", "classhb_1_1network_1_1_hb_tcp_client_config.html", null ],
        [ "hb::network::HbTcpServerConfig", "classhb_1_1network_1_1_hb_tcp_server_config.html", null ]
      ] ],
      [ "hb::network::HbTcpServerConfig", "classhb_1_1network_1_1_hb_tcp_server_config.html", null ]
    ] ],
    [ "hb::network::HbNetworkExchanges", "classhb_1_1network_1_1_hb_network_exchanges.html", null ],
    [ "hb::network::HbNetworkHeader", "classhb_1_1network_1_1_hb_network_header.html", null ],
    [ "hb::network::HbNetworkProtocol", "classhb_1_1network_1_1_hb_network_protocol.html", null ],
    [ "hb::network::HbNetworkUserData", "classhb_1_1network_1_1_hb_network_user_data.html", null ],
    [ "hb::network::HbNetworkUserInfo", "classhb_1_1network_1_1_hb_network_user_info.html", null ],
    [ "hb::tools::HbNullable< T >", "classhb_1_1tools_1_1_hb_nullable.html", null ],
    [ "hb::link::HbO2Config", "classhb_1_1link_1_1_hb_o2_config.html", [
      [ "hb::link::HbO2ClientConfig", "classhb_1_1link_1_1_hb_o2_client_config.html", null ],
      [ "hb::link::HbO2ServerConfig", "classhb_1_1link_1_1_hb_o2_server_config.html", null ]
    ] ],
    [ "HbPluginListWidget", null, [
      [ "hb::plugin::HbPluginListWidget", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html", null ]
    ] ],
    [ "hb::network::HbServiceConfig", "classhb_1_1network_1_1_hb_service_config.html", [
      [ "hb::network::HbServiceAuthConfig", "classhb_1_1network_1_1_hb_service_auth_config.html", [
        [ "hb::network::HbServiceAuthClientConfig", "classhb_1_1network_1_1_hb_service_auth_client_config.html", null ],
        [ "hb::network::HbServiceAuthServerConfig", "classhb_1_1network_1_1_hb_service_auth_server_config.html", null ]
      ] ],
      [ "hb::network::HbServiceChannelConfig", "classhb_1_1network_1_1_hb_service_channel_config.html", [
        [ "hb::network::HbServiceChannelClientConfig", "classhb_1_1network_1_1_hb_service_channel_client_config.html", null ],
        [ "hb::network::HbServiceChannelServerConfig", "classhb_1_1network_1_1_hb_service_channel_server_config.html", null ]
      ] ],
      [ "hb::network::HbServicePresenceConfig", "classhb_1_1network_1_1_hb_service_presence_config.html", [
        [ "hb::network::HbServicePresenceClientConfig", "classhb_1_1network_1_1_hb_service_presence_client_config.html", null ],
        [ "hb::network::HbServicePresenceServerConfig", "classhb_1_1network_1_1_hb_service_presence_server_config.html", null ]
      ] ]
    ] ],
    [ "hb::tools::HbSingleton< T >", "classhb_1_1tools_1_1_hb_singleton.html", null ],
    [ "hb::tools::HbSingleton< HbUidGenerator< T, C > >", "classhb_1_1tools_1_1_hb_singleton.html", [
      [ "hb::tools::HbUidGenerator< T, C >", "classhb_1_1tools_1_1_hb_uid_generator.html", null ]
    ] ],
    [ "hb::tools::HbSteadyDateTime", "classhb_1_1tools_1_1_hb_steady_date_time.html", null ],
    [ "hb::tools::HbUid< T, C, Z >", "classhb_1_1tools_1_1_hb_uid.html", null ],
    [ "hb::tools::HbUid< contractuid, CLASS_CONTRACT >", "classhb_1_1tools_1_1_hb_uid.html", [
      [ "hb::network::HbNetworkContract", "classhb_1_1network_1_1_hb_network_contract.html", [
        [ "hb::network::HbAuthRequestContract", "classhb_1_1network_1_1_hb_auth_request_contract.html", [
          [ "hb::network::HbAuthFacebookRequestContract", "classhb_1_1network_1_1_hb_auth_facebook_request_contract.html", null ]
        ] ],
        [ "hb::network::HbAuthStatusContract", "classhb_1_1network_1_1_hb_auth_status_contract.html", null ],
        [ "hb::network::HbKickContract", "classhb_1_1network_1_1_hb_kick_contract.html", null ],
        [ "hb::network::HbPresenceContract", "classhb_1_1network_1_1_hb_presence_contract.html", null ],
        [ "hb::network::HbPresenceStatusContract", "classhb_1_1network_1_1_hb_presence_status_contract.html", null ]
      ] ]
    ] ],
    [ "hb::tools::HbUid< loguid, CLASS_LOG, true >", "classhb_1_1tools_1_1_hb_uid.html", [
      [ "hb::log::HbLogAbstractInput", "classhb_1_1log_1_1_hb_log_abstract_input.html", [
        [ "hb::log::HbLogLocalSocketInput", "classhb_1_1log_1_1_hb_log_local_socket_input.html", null ],
        [ "hb::log::HbLogTcpSocketInput", "classhb_1_1log_1_1_hb_log_tcp_socket_input.html", null ],
        [ "hb::log::HbLogUdpSocketInput", "classhb_1_1log_1_1_hb_log_udp_socket_input.html", null ]
      ] ],
      [ "hb::log::HbLogAbstractOutput", "classhb_1_1log_1_1_hb_log_abstract_output.html", [
        [ "hb::log::HbLogConsoleOutput", "classhb_1_1log_1_1_hb_log_console_output.html", null ],
        [ "hb::log::HbLogFileOutput", "classhb_1_1log_1_1_hb_log_file_output.html", null ],
        [ "hb::log::HbLogGuiOutput", "classhb_1_1log_1_1_hb_log_gui_output.html", null ],
        [ "hb::log::HbLogLocalSocketOutput", "classhb_1_1log_1_1_hb_log_local_socket_output.html", null ],
        [ "hb::log::HbLogTcpSocketOutput", "classhb_1_1log_1_1_hb_log_tcp_socket_output.html", null ],
        [ "hb::log::HbLogUdpSocketOutput", "classhb_1_1log_1_1_hb_log_udp_socket_output.html", null ]
      ] ]
    ] ],
    [ "hb::tools::HbUid< networkuid, CLASS_SERVER, true >", "classhb_1_1tools_1_1_hb_uid.html", [
      [ "hb::network::HbAbstractServer", "classhb_1_1network_1_1_hb_abstract_server.html", [
        [ "hb::network::HbTcpServer", "classhb_1_1network_1_1_hb_tcp_server.html", null ]
      ] ],
      [ "hb::network::HbSocketHandler", "classhb_1_1network_1_1_hb_socket_handler.html", [
        [ "hb::network::HbTcpSocketHandler", "classhb_1_1network_1_1_hb_tcp_socket_handler.html", null ]
      ] ]
    ] ],
    [ "hb::tools::HbUid< networkuid, CLASS_SOCKET, true >", "classhb_1_1tools_1_1_hb_uid.html", [
      [ "hb::network::HbAbstractSocket", "classhb_1_1network_1_1_hb_abstract_socket.html", [
        [ "hb::network::HbTcpSocket", "classhb_1_1network_1_1_hb_tcp_socket.html", null ]
      ] ]
    ] ],
    [ "hb::tools::HbUid< replyuid, CLASS_REPLIES, true >", "classhb_1_1tools_1_1_hb_uid.html", [
      [ "hb::tools::HbTimeoutNetworkReply", "classhb_1_1tools_1_1_hb_timeout_network_reply.html", null ]
    ] ],
    [ "hb::network::IHbContractListener", "classhb_1_1network_1_1_i_hb_contract_listener.html", [
      [ "hb::network::HbAuthService", "classhb_1_1network_1_1_hb_auth_service.html", [
        [ "hb::network::HbClientAuthService", "classhb_1_1network_1_1_hb_client_auth_service.html", null ],
        [ "hb::network::HbServerAuthService", "classhb_1_1network_1_1_hb_server_auth_service.html", null ]
      ] ],
      [ "hb::network::HbServerPresenceService", "classhb_1_1network_1_1_hb_server_presence_service.html", null ]
    ] ],
    [ "hb::log::IHbLoggerInput", "classhb_1_1log_1_1_i_hb_logger_input.html", [
      [ "hb::log::HbLogAbstractInput", "classhb_1_1log_1_1_hb_log_abstract_input.html", null ]
    ] ],
    [ "hb::log::IHbLoggerOutput", "classhb_1_1log_1_1_i_hb_logger_output.html", [
      [ "hb::log::HbLogAbstractOutput", "classhb_1_1log_1_1_hb_log_abstract_output.html", null ]
    ] ],
    [ "hb::plugin::IHbPlugin", "classhb_1_1plugin_1_1_i_hb_plugin.html", null ],
    [ "hb::network::IHbSocketAuthListener", "classhb_1_1network_1_1_i_hb_socket_auth_listener.html", [
      [ "hb::network::HbPresenceService", "classhb_1_1network_1_1_hb_presence_service.html", [
        [ "hb::network::HbClientPresenceService", "classhb_1_1network_1_1_hb_client_presence_service.html", null ],
        [ "hb::network::HbServerPresenceService", "classhb_1_1network_1_1_hb_server_presence_service.html", null ]
      ] ]
    ] ],
    [ "hb::network::IHbSocketListener", "classhb_1_1network_1_1_i_hb_socket_listener.html", [
      [ "hb::network::HbAuthService", "classhb_1_1network_1_1_hb_auth_service.html", null ]
    ] ],
    [ "hb::network::IHbUserContractListener", "classhb_1_1network_1_1_i_hb_user_contract_listener.html", [
      [ "hb::network::HbChannelService", "classhb_1_1network_1_1_hb_channel_service.html", [
        [ "hb::network::HbClientChannelService", "classhb_1_1network_1_1_hb_client_channel_service.html", null ],
        [ "hb::network::HbServerChannelService", "classhb_1_1network_1_1_hb_server_channel_service.html", null ]
      ] ],
      [ "hb::network::HbNetworkChannel", "classhb_1_1network_1_1_hb_network_channel.html", [
        [ "hb::network::HbClientChannel", "classhb_1_1network_1_1_hb_client_channel.html", [
          [ "hb::network::HbClientPeopledChannel", "classhb_1_1network_1_1_hb_client_peopled_channel.html", null ]
        ] ],
        [ "hb::network::HbServerChannel", "classhb_1_1network_1_1_hb_server_channel.html", [
          [ "hb::network::HbServerPeopledChannel", "classhb_1_1network_1_1_hb_server_peopled_channel.html", null ]
        ] ]
      ] ]
    ] ],
    [ "hb::network::IHbUserListener", "classhb_1_1network_1_1_i_hb_user_listener.html", [
      [ "hb::network::HbChannelService", "classhb_1_1network_1_1_hb_channel_service.html", null ],
      [ "hb::network::HbServerPeopledChannel", "classhb_1_1network_1_1_hb_server_peopled_channel.html", null ]
    ] ],
    [ "hb::tools::ModelFilter", "classhb_1_1tools_1_1_model_filter.html", null ],
    [ "QDialog", null, [
      [ "hb::log::HbLogConfigDialog", "classhb_1_1log_1_1_hb_log_config_dialog.html", null ],
      [ "hb::log::HbLogWidget", "classhb_1_1log_1_1_hb_log_widget.html", null ]
    ] ],
    [ "QLocalServer", null, [
      [ "hb::log::HbLogLocalSocketInput", "classhb_1_1log_1_1_hb_log_local_socket_input.html", null ]
    ] ],
    [ "QLocalSocket", null, [
      [ "hb::log::HbLogLocalSocketOutput", "classhb_1_1log_1_1_hb_log_local_socket_output.html", null ]
    ] ],
    [ "QObject", null, [
      [ "hb::link::HbO2", "classhb_1_1link_1_1_hb_o2.html", [
        [ "hb::link::HbO2Client", "classhb_1_1link_1_1_hb_o2_client.html", [
          [ "hb::link::HbO2ClientFacebook", "classhb_1_1link_1_1_hb_o2_client_facebook.html", null ]
        ] ],
        [ "hb::link::HbO2Server", "classhb_1_1link_1_1_hb_o2_server.html", [
          [ "hb::link::HbO2ServerFacebook", "classhb_1_1link_1_1_hb_o2_server_facebook.html", null ]
        ] ]
      ] ],
      [ "hb::log::HbLogConfig", "classhb_1_1log_1_1_hb_log_config.html", null ],
      [ "hb::log::HbLoggerPool", "classhb_1_1log_1_1_hb_logger_pool.html", null ],
      [ "hb::log::HbLoggerStream", "classhb_1_1log_1_1_hb_logger_stream.html", [
        [ "hb::log::HbLoggerInputs", "classhb_1_1log_1_1_hb_logger_inputs.html", null ],
        [ "hb::log::HbLoggerOutputs", "classhb_1_1log_1_1_hb_logger_outputs.html", null ]
      ] ],
      [ "hb::log::HbLogGuiNotifier", "classhb_1_1log_1_1_hb_log_gui_notifier.html", null ],
      [ "hb::log::HbLogHandler", "classhb_1_1log_1_1_hb_log_handler.html", null ],
      [ "hb::log::HbLogManager", "classhb_1_1log_1_1_hb_log_manager.html", null ],
      [ "hb::log::HbLogMessage", "classhb_1_1log_1_1_hb_log_message.html", null ],
      [ "hb::network::HbAbstractNetwork", "classhb_1_1network_1_1_hb_abstract_network.html", [
        [ "hb::network::HbAbstractClient", "classhb_1_1network_1_1_hb_abstract_client.html", [
          [ "hb::network::HbTcpClient", "classhb_1_1network_1_1_hb_tcp_client.html", null ]
        ] ],
        [ "hb::network::HbAbstractServer", "classhb_1_1network_1_1_hb_abstract_server.html", null ]
      ] ],
      [ "hb::network::HbAbstractSocket", "classhb_1_1network_1_1_hb_abstract_socket.html", null ],
      [ "hb::network::HbAuthStrategy", "classhb_1_1network_1_1_hb_auth_strategy.html", [
        [ "hb::network::HbClientAuthStrategy", "classhb_1_1network_1_1_hb_client_auth_strategy.html", [
          [ "hb::network::HbClientAuthFacebookStrategy", "classhb_1_1network_1_1_hb_client_auth_facebook_strategy.html", null ]
        ] ],
        [ "hb::network::HbServerAuthStrategy", "classhb_1_1network_1_1_hb_server_auth_strategy.html", [
          [ "hb::network::HbServerAuthFacebookStrategy", "classhb_1_1network_1_1_hb_server_auth_facebook_strategy.html", null ]
        ] ]
      ] ],
      [ "hb::network::HbConnectionPool", "classhb_1_1network_1_1_hb_connection_pool.html", [
        [ "hb::network::HbClientConnectionPool", "classhb_1_1network_1_1_hb_client_connection_pool.html", null ],
        [ "hb::network::HbServerConnectionPool", "classhb_1_1network_1_1_hb_server_connection_pool.html", null ]
      ] ],
      [ "hb::network::HbNetworkService", "classhb_1_1network_1_1_hb_network_service.html", [
        [ "hb::network::HbAuthService", "classhb_1_1network_1_1_hb_auth_service.html", null ],
        [ "hb::network::HbChannelService", "classhb_1_1network_1_1_hb_channel_service.html", null ],
        [ "hb::network::HbNetworkChannel", "classhb_1_1network_1_1_hb_network_channel.html", null ],
        [ "hb::network::HbPresenceService", "classhb_1_1network_1_1_hb_presence_service.html", null ]
      ] ],
      [ "hb::network::HbNetworkUser", "classhb_1_1network_1_1_hb_network_user.html", null ],
      [ "hb::network::HbPeer", "classhb_1_1network_1_1_hb_peer.html", [
        [ "hb::network::HbClient", "classhb_1_1network_1_1_hb_client.html", null ],
        [ "hb::network::HbServer", "classhb_1_1network_1_1_hb_server.html", null ]
      ] ],
      [ "hb::network::HbSocketHandler", "classhb_1_1network_1_1_hb_socket_handler.html", null ],
      [ "hb::plugin::HbPluginInfos", "classhb_1_1plugin_1_1_hb_plugin_infos.html", null ],
      [ "hb::plugin::HbPluginManager", "classhb_1_1plugin_1_1_hb_plugin_manager.html", null ],
      [ "hb::plugin::HbPluginPlatform", "classhb_1_1plugin_1_1_hb_plugin_platform.html", null ],
      [ "hb::plugin::HbPluginService", "classhb_1_1plugin_1_1_hb_plugin_service.html", null ],
      [ "hb::tools::HbHttpRequester", "classhb_1_1tools_1_1_hb_http_requester.html", [
        [ "hb::link::HbFacebookRequester", "classhb_1_1link_1_1_hb_facebook_requester.html", null ]
      ] ],
      [ "hb::tools::HbTimeoutNetworkReplies", "classhb_1_1tools_1_1_hb_timeout_network_replies.html", null ]
    ] ],
    [ "QSortFilterProxyModel", null, [
      [ "hb::tools::HbMultipleSortFilterProxyModel", "classhb_1_1tools_1_1_hb_multiple_sort_filter_proxy_model.html", null ]
    ] ],
    [ "QTcpServer", null, [
      [ "hb::link::HbLinkLocalServer", "classhb_1_1link_1_1_hb_link_local_server.html", null ],
      [ "hb::log::HbLogTcpSocketInput", "classhb_1_1log_1_1_hb_log_tcp_socket_input.html", null ],
      [ "hb::network::TcpServer", "classhb_1_1network_1_1_tcp_server.html", null ]
    ] ],
    [ "QTcpSocket", null, [
      [ "hb::log::HbLogTcpSocketOutput", "classhb_1_1log_1_1_hb_log_tcp_socket_output.html", null ]
    ] ],
    [ "QTimer", null, [
      [ "hb::tools::HbTimeoutNetworkReply", "classhb_1_1tools_1_1_hb_timeout_network_reply.html", null ]
    ] ],
    [ "QUdpSocket", null, [
      [ "hb::log::HbLogUdpSocketInput", "classhb_1_1log_1_1_hb_log_udp_socket_input.html", null ],
      [ "hb::log::HbLogUdpSocketOutput", "classhb_1_1log_1_1_hb_log_udp_socket_output.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "hb::plugin::HbPluginListWidget", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html", null ]
    ] ]
];
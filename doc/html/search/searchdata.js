var indexSectionsWithContent =
{
  0: "abcefghilmnopqrstuw~",
  1: "himt",
  2: "h",
  3: "hi",
  4: "acefghilopqrstuw~",
  5: "c",
  6: "aclnrs",
  7: "o",
  8: "chiq",
  9: "bt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends",
  8: "Macros",
  9: "Pages"
};


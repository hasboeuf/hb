var searchData=
[
  ['q_5fdelete_5fptr',['q_delete_ptr',['../_hb_global_8h.html#a24c30edbaf334eecc259b6fc3267983e',1,'HbGlobal.h']]],
  ['qlist_5fdynamic_5fcast',['qlist_dynamic_cast',['../_hb_global_8h.html#affd64bec836c4dd54ba2ec79e80616b1',1,'HbGlobal.h']]],
  ['qscopename',['qScopeName',['../_hb_global_8h.html#a8dd57eb1c7b8f3cf851628d7626af57e',1,'qScopeName():&#160;HbGlobal.h'],['../_hb_global_8h.html#ad5575af777fa4cade8655c9b12a17a7d',1,'qScopeName(T *pointer):&#160;HbGlobal.h'],['../_hb_global_8h.html#a6c7ee21a942938e567c35d0904546dd5',1,'qScopeName(const T &amp;expression):&#160;HbGlobal.h']]],
  ['qtypename',['qTypeName',['../_hb_global_8h.html#a38420ad15a24e24dcc0aac5a5afafe80',1,'qTypeName():&#160;HbGlobal.h'],['../_hb_global_8h.html#a8be2ff557540d536214bfff4fcbf5180',1,'qTypeName(T *pointer):&#160;HbGlobal.h'],['../_hb_global_8h.html#a3ae225c11540e37a6f68f8d655787b3e',1,'qTypeName(const T &amp;expression):&#160;HbGlobal.h']]]
];

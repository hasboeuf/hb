var dir_b58d0ee721b2087e51cb0bcc78a10465 =
[
    [ "HbPlugin.h", "_hb_plugin_8h.html", "_hb_plugin_8h" ],
    [ "HbPluginInfos.h", "_hb_plugin_infos_8h.html", [
      [ "HbPluginInfos", "classhb_1_1plugin_1_1_hb_plugin_infos.html", "classhb_1_1plugin_1_1_hb_plugin_infos" ]
    ] ],
    [ "HbPluginListWidget.h", "_hb_plugin_list_widget_8h.html", [
      [ "HbPluginListWidget", "classhb_1_1plugin_1_1_hb_plugin_list_widget.html", "classhb_1_1plugin_1_1_hb_plugin_list_widget" ]
    ] ],
    [ "HbPluginManager.h", "_hb_plugin_manager_8h.html", [
      [ "HbPluginManager", "classhb_1_1plugin_1_1_hb_plugin_manager.html", "classhb_1_1plugin_1_1_hb_plugin_manager" ]
    ] ],
    [ "HbPluginPlatform.h", "_hb_plugin_platform_8h.html", [
      [ "HbPluginPlatform", "classhb_1_1plugin_1_1_hb_plugin_platform.html", "classhb_1_1plugin_1_1_hb_plugin_platform" ]
    ] ],
    [ "HbPluginService.h", "_hb_plugin_service_8h.html", [
      [ "HbPluginService", "classhb_1_1plugin_1_1_hb_plugin_service.html", "classhb_1_1plugin_1_1_hb_plugin_service" ]
    ] ],
    [ "IHbPlugin.h", "_i_hb_plugin_8h.html", [
      [ "IHbPlugin", "classhb_1_1plugin_1_1_i_hb_plugin.html", "classhb_1_1plugin_1_1_i_hb_plugin" ]
    ] ]
];
/****************************************************************************
** Copyright (C) 2012-2015 Adrien Gavignet (hasboeuf)
** Contact: adrien.gavignet@gmail.com
**
** This file is part of the Hb module. Provided "AS IS", WITHOUT WARRANTIES
** OR CONDITIONS OF ANY KIND, either express or implied.
****************************************************************************/

#ifndef HBLOGCONFIGDIALOG_H
#define HBLOGCONFIGDIALOG_H

/*! \file HbLogConfigDialog.h */

// Qt
#include <QButtonGroup>
// Local
#include <HbLog.h>
#include <gui/HbLogConfig.h>
#include <ui_HbLogConfigDialog.h>

namespace hb {
namespace log {
/*!
 * TODOC.
 */
class HbLogConfigDialog : public QDialog, private Ui::HbLogConfigDialog {
    Q_OBJECT
    Q_DISABLE_COPY(HbLogConfigDialog)

public:
    HbLogConfigDialog() = delete;
    HbLogConfigDialog(HbLogConfig& pConfig, QWidget* parent = nullptr);
    virtual ~HbLogConfigDialog() = default;

    const HbLogConfig& config() const;

private:
    void saveConfig();
    void updateGui();

    void onFontClicked();
    void onColorClicked(int color_id);
    void onBackgroundColorClicked();
    void onSaveClicked();
    void onResetClicked();
    void onImportClicked();
    void onExportClicked();

    HbLogConfig mConfig;

    QButtonGroup qbg_colors;
    QList<QLineEdit*> qfl_editor_fields; // Use when saving.
};
} // namespace log
} // namespace hb

#endif // HBLOGCONFIGDIALOG_H
